#!/usr/bin/python3
from Bio import Entrez
from Bio import SeqIO
from reportlab.lib import colors
from reportlab.lib.units import cm
from Bio.Graphics import GenomeDiagram
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.GenBank import RecordParser
parser = RecordParser()

def mapConstruct(inFile, outFile):
    seq_record = SeqIO.read(
                    inFile, 
                    "gb"
                )
                
    gb_rec = parser.parse(open(inFile))
    constructType = gb_rec.residue_type
    
    print(seq_record)
    print(seq_record.seq)
    print(
        "%s with %i features" % (
            seq_record.id, 
            len(seq_record.features)
            )
        )

    gd_diagram = GenomeDiagram.Diagram(seq_record.id)
    gd_track_for_features = gd_diagram.new_track(1, name="Annotated Features")
    gd_feature_set = gd_track_for_features.new_set()

    for feature in seq_record.features:
        if feature.type == "CDS":
            if len(gd_feature_set) % 2 == 0:
                color = colors.blue
            else:
                color = colors.lightblue
            gd_feature_set.add_feature(
                feature, 
                sigil="ARROW",
                color=color, 
                label=True,
                label_size=14, 
                label_position="center"
                )
        else:
            if len(gd_feature_set) % 2 == 0:
                color = colors.indigo
            else:
                color = colors.lightcoral
            gd_feature_set.add_feature(
                feature, 
                sigil="BOX",
                color=color, 
                label=True,
                label_size=14,
                label_position="center"
                )
    
    #I want to include some strandless features, so for an example
    #will use EcoRI recognition sites etc.
    # for site, name, color in [("GAATTC","EcoRI",colors.green),
    #                           ("CCCGGG","SmaI",colors.orange),
    #                           ("AAGCTT","HindIII",colors.red),
    #                           ("GGATCC","BamHI",colors.purple)]:
    #     index = 0
    #     while True:
    #         index  = seq_record.seq.find(
    #             site, 
    #             start=index
    #             )
    #         if index == -1 : break
    #         feature = SeqFeature(
    #             FeatureLocation(
    #                 index, 
    #                 index+len(site)
    #                 )
    #             )
    #         gd_feature_set.add_feature(
    #             feature, 
    #             color=color, 
    #             name=name,
    #             label=True, 
    #             label_size = 20,
    #             label_color=color
    #             )
    #         index += len(site)
    
    if constructType == "DNA     circular":
        ## Circular
        gd_diagram.draw(
            format="circular",
            circular=True,
            pagesize=(30*cm,30*cm),
            start=0, 
            end=len(seq_record), 
            circle_core = 0.65
            )
        gd_diagram.write(outFile, "PDF")
    else:
        ## Linear
        gd_diagram.draw(
            format="linear", 
            pagesize='A4', 
            fragments=4,
            start=0, 
            end=len(seq_record)
            )
            
        gd_diagram.write(outFile, "PDF")
        
