
library(reticulate)

source_python('python/mapConstruct.py')

mapConstruct(
    "data/sequenceExample.gb",
    "outputs/plasmidExample.pdf"
)
